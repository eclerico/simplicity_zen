/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.my_custom_behavior = {
	  attach: function(context, settings) {

	    // show/hide the mobile menu based on class added to container
		$('.menu-icon').click(function(){
			$(this).parent().toggleClass('is-tapped');
			 return false;
		});

	  	// handle touch device events on drop down, first tap adds class, second navigates
	 	$('.touch #block-system-main-menu li.nav-dropdown > a').on('touchend',
			function(e){
		    if ($('.menu-icon').is(':hidden')) {
			  	var parent = $(this).parent();
				  $(this).find('.clicked').removeClass('clicked');
		 			if (parent.hasClass('clicked')) {
						 window.location.href = $(this).attr('href');
					} else {
		 				$(this).addClass('linkclicked');
		 				
	          // close other open menus at this level
		 			  $(this).parent().parent().find('.clicked').removeClass('clicked');
	          
	          parent.addClass('clicked');
						e.preventDefault();
					}
				}	
		});
	 	
	 	// handle the expansion of mobile menu drop down nesting
	   	$('#block-system-main-menu li.nav-dropdown').click(
	  		function(event){
	  			if(event.stopPropagation) {
	  				event.stopPropagation();
	  			} else {
	  				event.cancelBubble = true;	
	  			}
	  			
	  			if ($('.menu-icon').is(':visible')) {
	  				$(this).find('> ul').toggle();
	  				$(this).toggleClass('expanded');
	  			}
	  		 }
	  	);
	  	
	  	
	  	// prevent links for propagating click/tap events that may trigger hiding/unhiding 
	  	$('#block-system-main-menu a.nav-dropdown, #block-system-main-menu li.nav-dropdown a').click(
	  		function(event){
	 			if(event.stopPropagation) {
	 				event.stopPropagation();
	 			} else {
	 				event.cancelBubble = true;	
	 			}
	  		}
	  	);
	  	
	  	// javascript fade in and out of dropdown menu
	  	$('.no-touch #block-system-main-menu li').hover(
		  	function() {
		  		if (!$('.menu-icon').is(':visible')) {
		  		 	$(this).find('> ul').fadeIn(150);
		  		}
		  	},
		  	function() {
		  		if (!$('.menu-icon').is(':visible')) {
		  			 $(this).find('> ul').fadeOut(150);
		  		}
		  	}	
	  	);

	  }
	};


})(jQuery, Drupal, this, this.document);
