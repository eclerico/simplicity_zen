/*
Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

/*
 * This file is used/requested by the 'Styles' button.
 * The 'Styles' button is not enabled by default in DrupalFull and DrupalFiltered toolbars.
 */
if(typeof(CKEDITOR) !== 'undefined') {
    CKEDITOR.addStylesSet( 'drupal',
    [
            /* Block Styles */

            // These styles are already available in the "Format" drop-down list, so they are
            // not needed here by default. You may enable them to avoid placing the
            // "Format" drop-down list in the toolbar, maintaining the same features.
            /*
            { name : 'Paragraph'        , element : 'p' },
            { name : 'Heading 1'        , element : 'h1' },
            { name : 'Heading 2'        , element : 'h2' },
            { name : 'Heading 3'        , element : 'h3' },
            { name : 'Heading 4'        , element : 'h4' },
            { name : 'Heading 5'        , element : 'h5' },
            { name : 'Heading 6'        , element : 'h6' },
            { name : 'Preformatted Text', element : 'pre' },
            { name : 'Address'          , element : 'address' },
            */

            { name : 'Headline'       , element : 'h2', attributes : { 'class' : 'simplicity-content-light-headline' } },
            { name : 'Script'      , element : 'h2', attributes : { 'class' : 'simplicity-content-script-headline' } },

            /* Inline Styles */

            // { name : 'Big'               , element : 'big' },
            // { name : 'Small'         , element : 'small' },
            // { name : 'Typewriter'        , element : 'tt' },

            /* Object Styles */

            {
                    name : 'Left Image',
                    element : 'img',
                    attributes :
                    {
                            'class' : 'simplicity-content-image-left'
                    }
            },

            {
                    name : 'Center Image',
                    element : 'img',
                    attributes :
                    {
                            'class' : 'simplicity-content-image-center'
                    }
            },

            {
                    name : 'Right Image',
                    element : 'img',
                    attributes :
                    {
                            'class' : 'simplicity-content-image-right'
                    }
            },

            {
                    name : 'Left Iframe',
                    element : 'iframe',
                    attributes :
                    {
                            'class' : 'simplicity-content-image-left'
                    }
            },

            {
                    name : 'Center Iframe',
                    element : 'iframe',
                    attributes :
                    {
                            'class' : 'simplicity-content-image-center'
                    }
            },

            {
                    name : 'Right Iframe',
                    element : 'iframe',
                    attributes :
                    {
                            'class' : 'simplicity-content-image-right'
                    }
            },

            {
                    name : 'Clear left',
                    element : 'p',
                    attributes :
                    {
                            'class' : 'clear-left'
                    }
            },
            {
                    name : 'Clear right',
                    element : 'p',
                    attributes :
                    {
                            'class' : 'clear-right'
                    }
            },

            { name : 'Green Button', element : 'a', attributes : { 'class' : 'sassy-button-green' }},
            { name : 'Blue Button', element : 'a', attributes : { 'class' : 'sassy-button-blue' }},
            { name : 'Orange Button', element : 'a', attributes : { 'class' : 'sassy-button-orange' }},
            { name : 'Gray Button', element : 'a', attributes : { 'class' : 'sassy-button-gray' }},
    ]);
}